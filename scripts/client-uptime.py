#!/usr/bin/env python

__author__ = "Patiga"
__license__ = "AGPL-3.0-only"
__version__ = "0.1.0"
__email__ = "dev@patiga.eu"

""" Generate a map which displays the client's uptime
Intended as a entities background, can also be appended to other maps.
The produced map is saved as as "Client-Uptime.map" in the current directory.
"""

import twmap

map = twmap.Map.empty("DDNet06")
map.info.author = __author__
map.info.license = "CC-BY"
map.info.version = __version__
map.info.credits = "Patiga's twmap-py script 'client-uptime'"

map.images.new_from_ddnet("font_teeworlds.png")

def anim_envelope(map: twmap.Map, name, visible, intervalls) -> None:
    envelope = map.envelopes.new("Color")
    # envelope.synchronized = True
    envelope.name = name
    p0 = envelope.points.new(0)
    p0.curve = "Step"
    p1 = envelope.points.new(visible)
    p1.a = 0
    p1.curve = "Step"
    p2 = envelope.points.new(int(visible * intervalls))
    p2.a = 0
    p2.curve = "Step"

# cycle_intervalls: how many intervalls until it starts from the start
# relevant_intervalls: intervalls in total this should be visible
# total_intervalls is the new intervalls
def irregular_anim_envelope(map: twmap.Map, name, visible, cycle_intervalls, first_relevant_intervall, last_relevant_intervall, total_intervalls) -> None:
    envelope = map.envelopes.new("Color")
    # envelope.synchronized = True
    envelope.name = name
    for i in range(first_relevant_intervall, last_relevant_intervall, cycle_intervalls):
        p0 = envelope.points.new(i * visible)
        p0.curve = "Step"
        p1 = envelope.points.new((i * visible + visible))
        p1.a = 0
        p1.curve = "Step"
    p2 = envelope.points.new(visible * total_intervalls)
    p2.a = 0
    p2.curve = "Step"

SECOND = 1000
MIN = SECOND * 60
HOUR = MIN * 60
DAY = HOUR * 24
ONE_SECOND_FIELD = { "name": "One-Seconds", "visibility": SECOND, "digits": 10, "offset": 0 }
TEN_SECOND_FIELD = { "name": "Ten-Seconds", "visibility": SECOND * 10, "digits": 6, "offset": 1 }
ONE_MINUTE_FIELD = { "name": "One-Minutes", "visibility": MIN, "digits": 10, "offset": 2.5 }
TEN_MINUTE_FIELD = { "name": "Ten-Minutes", "visibility": MIN * 10, "digits": 6, "offset": 3.5, "lazy": True }
ONE_HOUR_FIELD = { "name": "One-Hours", "visibility": HOUR, "digits": 24, "offset": 6, "lazy": True }
TEN_HOUR_FIELD_LOWER = { "name": "L One-Hours", "visibility": HOUR * 10, "digits": 2.4, "offset": 7, "lazy": True }
TEN_HOUR_FIELD_UPPER = { "name": "U One-Hours", "visibility": HOUR * 4, "digits": 6, "offset": 7, "lazy": True }

# the regular fields
FIELDS = [ONE_SECOND_FIELD, TEN_SECOND_FIELD, ONE_MINUTE_FIELD, TEN_MINUTE_FIELD]
DIGIT_WIDTH = 0.7
TIME_END = 2**31 - 1

position_env = len(map.envelopes)
position_envelope = map.envelopes.new("Position")
position_envelope.name = "Time Formatting"
points = position_envelope.points
p0 = points.new(0)
p0.curve = "Step"
start = ONE_MINUTE_FIELD["offset"]
p1 = points.new(MIN * 10)
p1.x = int((TEN_MINUTE_FIELD["offset"] - start) * DIGIT_WIDTH)
p1.curve = "Step"
p2 = points.new(HOUR)
p2.x = int((ONE_HOUR_FIELD["offset"] - start) * DIGIT_WIDTH)
p2.curve = "Step"
p3 = points.new(HOUR * 10)
p3.x = int((TEN_HOUR_FIELD_LOWER["offset"] - start) * DIGIT_WIDTH)
p3.curve = "Step"
p4 = points.new(TIME_END)
p4.x = p3.x
p4.curve = "Step"

for field in FIELDS:
    field["color_env"] = len(map.envelopes)
    anim_envelope(map, field["name"], field["visibility"], field["digits"])

one_hour_color_env_zero = len(map.envelopes)
irregular_anim_envelope(map, "0" + ONE_HOUR_FIELD["name"], ONE_HOUR_FIELD["visibility"], 10, 10, 24, 24)
one_hour_color_env_lower = len(map.envelopes)
irregular_anim_envelope(map, "L" + ONE_HOUR_FIELD["name"], ONE_HOUR_FIELD["visibility"], 10, 0, 24, 24)
one_hour_color_env_upper = len(map.envelopes)
irregular_anim_envelope(map, "U" + ONE_HOUR_FIELD["name"], ONE_HOUR_FIELD["visibility"], 10, 0, 20, 24)

ten_hour_color_env_lower = len(map.envelopes)
anim_envelope(map, TEN_HOUR_FIELD_LOWER["name"], TEN_HOUR_FIELD_LOWER["visibility"], TEN_HOUR_FIELD_LOWER["digits"])
ten_hour_color_env_upper = len(map.envelopes)
anim_envelope(map, TEN_HOUR_FIELD_UPPER["name"], TEN_HOUR_FIELD_UPPER["visibility"], TEN_HOUR_FIELD_UPPER["digits"])

visibility_env = len(map.envelopes)
visibility_envelope = map.envelopes.new("Color")
visibility_envelope.name = "Visibility"
visibility_envelope.points.new(0)
p = visibility_envelope.points.new(TIME_END)
p.a = 0

group = map.groups.new()

group.parallax_x = 0
group.parallax_y = 0

layer = group.layers.new_quads()
layer.image = 0

UV_TILE = 1 / 16
CORNERS = [(0, 0), (1, 0), (0, 1), (1, 1)]
def tile_uv(tile_id):
    assert(tile_id >= 0 and tile_id < 256)
    x = tile_id % 16
    y = tile_id // 16
    return [((x + c[0]) * UV_TILE, (y + c[1]) * UV_TILE) for c in CORNERS]

DIGIT_ID = 54
DIGIT_OFFSETS = [9] + list(range(9))
DIGITS = [DIGIT_ID + offset for offset in DIGIT_OFFSETS]
POS_SEC_X = -20
POS_SEC_Y = 12
def insert_quad(layer: twmap.Layer, digit_offset, tile_id, offset, color_env, visibility, digits, lazy) -> None:
    quad = layer.quads.new(POS_SEC_X - DIGIT_WIDTH * offset, POS_SEC_Y, 1, 1)
    quad.texture_coords = tile_uv(tile_id)
    quad.color_env = color_env
    drop_in = int(lazy and digit_offset == 0) * visibility * digits
    quad.color_env_offset = int(-digit_offset * visibility - drop_in)
    quad.position_env = position_env


for field in FIELDS:
    for (i, digit_id) in enumerate(DIGITS[:field["digits"]]):
        insert_quad(layer, i, digit_id, field["offset"], field["color_env"], field["visibility"], field["digits"], "lazy" in field)

# ':' between seconds and minutes
quad = layer.quads.new(POS_SEC_X - (ONE_MINUTE_FIELD["offset"] + TEN_SECOND_FIELD["offset"]) / 2 + 0.5, POS_SEC_Y, 1, 1)
quad.texture_coords = tile_uv(64)
quad.position_env = position_env

# One-Hours
insert_quad(layer, 0, DIGITS[0], ONE_HOUR_FIELD["offset"], one_hour_color_env_zero, ONE_HOUR_FIELD["visibility"], ONE_HOUR_FIELD["digits"], False)
for (i, digit_id) in enumerate(DIGITS[1:4]):
    i += 1
    insert_quad(layer, i, digit_id, ONE_HOUR_FIELD["offset"], one_hour_color_env_lower, ONE_HOUR_FIELD["visibility"], ONE_HOUR_FIELD["digits"], ONE_HOUR_FIELD["lazy"])
for (i, digit_id) in enumerate(DIGITS[4:]):
    i += 4
    insert_quad(layer, i, digit_id, ONE_HOUR_FIELD["offset"], one_hour_color_env_upper, ONE_HOUR_FIELD["visibility"], ONE_HOUR_FIELD["digits"], ONE_HOUR_FIELD["lazy"])

# Ten-Hours
field = TEN_HOUR_FIELD_LOWER
insert_quad(layer, 1, DIGITS[1], field["offset"], ten_hour_color_env_lower, field["visibility"], field["digits"], field["lazy"])
field = TEN_HOUR_FIELD_UPPER
insert_quad(layer, 5, DIGITS[2], field["offset"], ten_hour_color_env_upper, field["visibility"], field["digits"], field["lazy"])

# 'H' hours marker
quad = layer.quads.new(POS_SEC_X - 3.5, POS_SEC_Y, 1, 1)
quad.texture_coords = tile_uv(8)
quad.color_env = visibility_env
quad.color_env_offset = -HOUR
quad.position_env = position_env

# Add proper physics group to make it a normal map
physics_group = map.groups.new_physics()
physics_group.layers.new_game(2, 2)

map.save("Client-Uptime.map")

