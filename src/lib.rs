use envelope_points::{
    ColorEnvPoint, ColorEnvPoints, PosEnvPoint, PosEnvPoints, SoundEnvPoint, SoundEnvPoints,
};
use quads::{PyQuad, PyQuads};
use sound_sources::{PySource, PySources};
use twmap::convert::TryTo;
use twmap::{Error, Layer, PhysicsLayer, TwMap, Version};

use crate::groups::{GroupData, PyGroup, PyGroups};
use crate::info::PyInfo;
use crate::sequence_wrapping::{MapNavigating, SequenceInitializer};

use envelopes::{EnvelopeData, PyEnvelope, PyEnvelopes};
use images::{PyImage, PyImages};
use layers::{PyLayer, PyLayers};
use sounds::{PySound, PySounds};

use fixed::traits::{Fixed, FromFixed, ToFixed};
use pyo3::prelude::*;
use pyo3::types::PyString;
use pyo3::{create_exception, PyNativeType};
use vek::Rgba;

use std::fmt::Display;
use std::ops::DerefMut;
use std::sync::{Arc, Mutex};

mod info;
#[macro_use]
mod sequence_wrapping;
mod envelope_points;
mod envelopes;
mod groups;
mod images;
mod layers;
mod quads;
mod sound_sources;
mod sounds;

create_exception!(module, MapError, pyo3::exceptions::PyException);

#[pymodule]
#[pyo3(name = "twmap")]
fn twmap_module(py: Python, m: &Bound<'_, PyModule>) -> PyResult<()> {
    m.add_class::<PyMap>()?;
    m.add("MapError", py.get_type_bound::<MapError>())?;
    m.add_function(wrap_pyfunction!(camera_dimensions, m)?)?;
    m.add_function(wrap_pyfunction!(max_camera_dimensions, m)?)?;

    m.add_class::<PyInfo>()?;
    m.add_class::<PyImages>()?;
    m.add_class::<PyImage>()?;
    m.add_class::<PySounds>()?;
    m.add_class::<PySound>()?;

    m.add_class::<PyEnvelopes>()?;
    m.add_class::<PyEnvelope>()?;
    m.add_class::<PosEnvPoints>()?;
    m.add_class::<PosEnvPoint>()?;
    m.add_class::<ColorEnvPoints>()?;
    m.add_class::<ColorEnvPoint>()?;
    m.add_class::<SoundEnvPoints>()?;
    m.add_class::<SoundEnvPoint>()?;

    m.add_class::<PyGroups>()?;
    m.add_class::<PyGroup>()?;
    m.add_class::<PyLayers>()?;
    m.add_class::<PyLayer>()?;
    m.add_class::<PyQuads>()?;
    m.add_class::<PyQuad>()?;
    m.add_class::<PySources>()?;
    m.add_class::<PySource>()?;

    Ok(())
}

#[pyclass(name = "Map")]
/// Main struct of twmap.
/// The constructor takes a file path.
///
/// Behavior you need to know about:
///     - Use the help on all objects of this module, it will give you thorough documentation!
///         ALSO THE COLLECTION TYPES HAVE DOCUMENTATION! (map.groups, layer.quads, etc.)
///         Only there you will find constructors for the different objects!
///         The help documentation is also the only place where the attributes are listed!
///     - If any attribute returns you a list, you will not be able to edit it in-place.
///         Bind it to a variable, edit the list there, put the list back into that attribute
///
/// Behavior of collection types:
///     - There are many different collection types in by this module, e.g. Groups, Envelopes, etc.
///     - They all have constructor methods you can find in their documentation
///         -> use `help` on an object of that type
///     - They all support the following functions:
///         `len(collection)` returns the length
///         `collection[index]` returns the corresponding element
///         `collection[index] = item` sets the corresponding element, returns the inserted object
///         `del collection[index]` deletes the corresponding element
///         `item in collection` returns if the item is part of the collection
///         `collection.append(item)` appends the item to the collection, returns the inserted object
///         `collection.insert(index, item)` inserts the item at the index (not replacing),
///             returns the newly inserted object
///     - Most have a `new` method (see their documentation), which appends a new item and returns it
///
/// Other behavior:
///     - Constructors will always return the constructed object
///         Also new objects are appended to the end
///     - All python objects can be compared if they are equal
///         This returns if they point to the same layer, not if the underlying layer is equal
///     - Some high-level methods on the struct (like rotate, optimize) will invalidate all
///         objects from the map (like groups, layers, images) which you have bound to variables
///     - You can copy objects from one map into another
///
/// Attributes:
///     info
///     groups
///     envelopes
///     images
///     sounds
#[derive(Clone)]
pub struct PyMap(Arc<Mutex<Map>>);

struct Map {
    map: Arc<Mutex<TwMap>>,
    groups: SequenceInitializer<GroupData, ()>,
    envelopes: SequenceInitializer<EnvelopeData, ()>,
    images: SequenceInitializer<(), ()>,
    sounds: SequenceInitializer<(), ()>,
}

pub fn py_err<T: Into<Error>>(err: T) -> PyErr {
    let err: Error = err.into();
    match err {
        Error::Io(err) => pyo3::exceptions::PyIOError::new_err(err.to_string()),
        _ => MapError::new_err(err.to_string()),
    }
}

pub fn map_err<T: Display>(err: T) -> PyErr {
    MapError::new_err(err.to_string())
}

impl Map {
    fn invalidate_lookup_tables(&mut self) {
        let map = self.map.lock().unwrap();
        if let Some(seq) = self.groups.try_get() {
            seq.lock().unwrap().invalidate(map.groups.len())
        }
        if let Some(seq) = self.envelopes.try_get() {
            seq.lock().unwrap().invalidate(map.envelopes.len())
        }
        if let Some(seq) = self.images.try_get() {
            seq.lock().unwrap().invalidate(map.images.len())
        }
        if let Some(seq) = self.sounds.try_get() {
            seq.lock().unwrap().invalidate(map.sounds.len())
        }
    }

    fn from_twmap(map: TwMap) -> Self {
        let arc_map = Arc::new(Mutex::new(map));
        Map {
            map: arc_map,
            groups: SequenceInitializer::default(),
            envelopes: SequenceInitializer::default(),
            images: SequenceInitializer::default(),
            sounds: SequenceInitializer::default(),
        }
    }

    fn into_py_map(self) -> PyMap {
        PyMap(Arc::new(Mutex::new(self)))
    }
}

#[pymethods]
impl PyMap {
    #[new]
    fn new(path: &str) -> PyResult<Self> {
        TwMap::parse_path(path)
            .map(Map::from_twmap)
            .map(Map::into_py_map)
            .map_err(py_err)
    }

    fn __getattr__(&self, name: &Bound<'_, PyString>) -> PyResult<PyObject> {
        let mut py_map = self.0.lock().unwrap();
        py_map.getattr(name)
    }

    /// Constructs an empty map object with the specified version.
    /// The parameter 'version' must be one of "DDNet06", "Teeworlds07".
    #[staticmethod]
    fn empty(version: &str) -> PyResult<Self> {
        let version = match version {
            "Teeworlds07" => Version::Teeworlds07,
            "DDNet06" => Version::DDNet06,
            _ => {
                return Err(pyo3::exceptions::PyValueError::new_err(
                    "Not a valid version, try 'Teeworlds07' or 'DDNet06'",
                ))
            }
        };
        Ok(Map::from_twmap(TwMap::empty(version)).into_py_map())
    }

    /// Save the map to the passed file path
    fn save(&self, path: &str) -> PyResult<()> {
        let mut data = Vec::new();
        self.0
            .lock()
            .unwrap()
            .map
            .lock()
            .unwrap()
            .save(&mut data)
            .map_err(py_err)?;
        std::fs::write(path, &data)?;
        Ok(())
    }

    /// Save the map in the MapDir format the passed file path
    fn save_dir(&self, path: &str) -> PyResult<()> {
        self.0
            .lock()
            .unwrap()
            .map
            .lock()
            .unwrap()
            .save_dir(path)
            .map_err(py_err)
    }

    /// Embed the external images using the mapres in the provided directory
    #[pyo3(signature = (mapres_directory=None))]
    fn embed_images(&self, mapres_directory: Option<&str>) -> PyResult<()> {
        let py_map = self.0.lock().unwrap();
        let mut map = py_map.map.lock().unwrap();
        if let Some(dir) = mapres_directory {
            map.embed_images(dir)
        } else {
            map.embed_images_auto()
        }
        .map_err(map_err)
    }

    /// Rotates the map clockwise
    fn rotate(&mut self) -> PyResult<()> {
        let mut py_map = self.0.lock().unwrap();
        {
            let mut map = py_map.map.lock().unwrap();
            map.load().map_err(map_err)?;
            match map.clone().rotate_right() {
                None => {
                    return Err(pyo3::exceptions::PyValueError::new_err(
                        "Overflow occurred while rotating",
                    ))
                }
                Some(rotated) => *map.deref_mut() = rotated,
            }
        }
        py_map.invalidate_lookup_tables();
        Ok(())
    }

    /// Mirrors the map along the horizontal axis (left <-> right)
    fn mirror(&mut self) -> PyResult<()> {
        let mut py_map = self.0.lock().unwrap();
        {
            let mut map = py_map.map.lock().unwrap();
            map.load().map_err(map_err)?;
            match map.clone().mirror() {
                None => {
                    return Err(pyo3::exceptions::PyValueError::new_err(
                        "Overflow occurred while mirroring",
                    ))
                }
                Some(mirrored) => *map.deref_mut() = mirrored,
            }
        }
        py_map.invalidate_lookup_tables();
        Ok(())
    }

    /// Returns 'DDNet06' or 'Teeworlds07', depending on the parsed map
    fn version(&self) -> &'static str {
        match self.0.lock().unwrap().map.lock().unwrap().version {
            Version::DDNet06 => "DDNet06",
            Version::Teeworlds07 => "Teeworlds07",
        }
    }

    /// Returns the physics group, `None` if there isn't one
    fn physics_group(&self, py: Python) -> PyResult<Option<PyGroup>> {
        let group_index = {
            let py_map = self.0.lock().unwrap();
            let map = py_map.map.lock().unwrap();
            match map.groups.iter().position(|g| g.is_physics_group()) {
                None => return Ok(None),
                Some(index) => index,
            }
        };
        self.__getattr__(&PyString::new_bound(py, "groups"))?
            .extract::<PyGroups>(py)?
            .__getitem__(group_index.try_to())
            .map(Some)
    }

    /// Returns the game layer (this shouldn't fail since every map has one)
    fn game_layer(&self, py: Python) -> PyResult<Option<PyLayer>> {
        get_py_physics_layer::<twmap::GameLayer>(self, py)
    }

    /// Returns the front layer or None
    fn front_layer(&self, py: Python) -> PyResult<Option<PyLayer>> {
        get_py_physics_layer::<twmap::FrontLayer>(self, py)
    }

    /// Returns the tele layer or None
    fn tele_layer(&self, py: Python) -> PyResult<Option<PyLayer>> {
        get_py_physics_layer::<twmap::TeleLayer>(self, py)
    }

    /// Returns the speedup layer or None
    fn speedup_layer(&self, py: Python) -> PyResult<Option<PyLayer>> {
        get_py_physics_layer::<twmap::SpeedupLayer>(self, py)
    }

    /// Returns the switch layer or None
    fn switch_layer(&self, py: Python) -> PyResult<Option<PyLayer>> {
        get_py_physics_layer::<twmap::SwitchLayer>(self, py)
    }

    /// Returns the tune layer or None
    fn tune_layer(&self, py: Python) -> PyResult<Option<PyLayer>> {
        get_py_physics_layer::<twmap::TuneLayer>(self, py)
    }
}

impl Map {
    fn getattr(&mut self, name: &Bound<'_, PyString>) -> PyResult<PyObject> {
        let py = name.py();
        let name = name.to_cow()?;
        let map = self.map.lock().unwrap();
        Ok(match name.as_ref() {
            "info" => PyInfo {
                map: self.map.clone(),
            }
            .into_py(py),
            "groups" => {
                PyGroups(self.groups.get(map.groups.len(), self.map.clone(), ())).into_py(py)
            }
            "envelopes" => PyEnvelopes(self.envelopes.get(
                map.envelopes.len(),
                self.map.clone(),
                (),
            ))
            .into_py(py),
            "images" => {
                PyImages(self.images.get(map.images.len(), self.map.clone(), ())).into_py(py)
            }
            "sounds" => {
                PySounds(self.sounds.get(map.sounds.len(), self.map.clone(), ())).into_py(py)
            }
            _ => {
                return Err(pyo3::exceptions::PyAttributeError::new_err(format!(
                    "The twmap.Map object has no attribute '{}'",
                    name
                )))
            }
        })
    }
}

fn get_py_physics_layer<T: PhysicsLayer>(py_map: &PyMap, py: Python) -> PyResult<Option<PyLayer>> {
    let physics_group = match py_map.physics_group(py)? {
        None => return Ok(None),
        Some(g) => g,
    };
    let layers = physics_group
        .__getattr__(&PyString::new_bound(py, "layers"))?
        .extract::<PyLayers>(py)?;
    let mut index = 0;
    while let Ok(layer) = layers.__getitem__(index) {
        if Layer::access(&layer, |l| Ok(l.kind() == T::kind()))? {
            return Ok(Some(layer));
        }
        index += 1;
    }
    Ok(None)
}

fn fixed_py<T: Fixed>(n: T) -> f64 {
    f64::from_fixed(n)
}

fn py_fixed<T: Fixed + Display>(f: f64) -> PyResult<T> {
    f.checked_to_fixed().ok_or_else(|| {
        pyo3::exceptions::PyValueError::new_err(format!(
            "Value {} is too large, it must be between {} and {}",
            f,
            T::MIN,
            T::MAX
        ))
    })
}

pub fn point_to_tup<T: Fixed, U: AsRef<[T]>>(p: U) -> (f64, f64)
where
    f64: From<T>,
{
    let slice = p.as_ref();
    (fixed_py(slice[0]), fixed_py(slice[1]))
}

pub fn tup_to_point<T: Fixed, U>(t: (f64, f64)) -> PyResult<U>
where
    U: From<(T, T)>,
{
    Ok((py_fixed(t.0)?, py_fixed(t.1)?).into())
}

pub fn depy2<T: Fixed, U>(value: PyObject, py: Python) -> PyResult<U>
where
    U: From<(T, T)>,
{
    let tup = value.extract(py)?;
    tup_to_point(tup)
}

pub fn py_col(color: Rgba<u8>) -> (u8, u8, u8, u8) {
    (color.r, color.g, color.b, color.a)
}

pub fn col_from_tuple(tup: (u8, u8, u8, u8)) -> PyResult<Rgba<u8>> {
    Ok(Rgba {
        r: tup.0,
        g: tup.1,
        b: tup.2,
        a: tup.3,
    })
}

pub fn depy_col(value: PyObject, py: Python) -> PyResult<Rgba<u8>> {
    let tup = value.extract(py)?;
    col_from_tuple(tup)
}

/// The aspect ratio is width / height.
/// Returns the dimension in tiles that Teeworlds/DDNet would render in that aspect ratio.
/// Default zoom level is assumed.
#[pyfunction]
fn camera_dimensions(aspect_ratio: f32) -> (f32, f32) {
    let dimensions = twmap::edit::camera_dimensions(aspect_ratio);
    (dimensions.w, dimensions.h)
}

/// The maximum amount of tiles horizontally and vertically that the camera can cover.
/// Note that it can never be the maximum in both directions.
/// Default zoom level is assumed.
#[pyfunction]
fn max_camera_dimensions() -> (f64, f64) {
    let max = twmap::edit::MAX_CAMERA_DIMENSIONS;
    (fixed_py(max.w), fixed_py(max.h))
}
