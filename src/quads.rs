use twmap::{AnyLayer, Layer, Quad, QuadsLayer, TwMap};

use crate::layers::PyLayer;
use crate::sequence_wrapping::*;
use crate::{col_from_tuple, depy2, point_to_tup, py_col, tup_to_point};

use pyo3::{prelude::*, types::PyString};

use std::convert::TryFrom;
use std::fmt;
use std::sync::{Arc, Mutex};

#[pyclass(name = "Quads")]
#[derive(Clone)]
pub struct PyQuads(pub Arc<Mutex<SequenceIndex<(), PyLayer>>>);

/// Quad object of the Quads layer
/// All positions are (x, y) tuples. (1.5, 8) => 1.5 tiles right of origin, 8 tiles down
/// The four corners are always in the order top-left -> top-right -> bottom-left -> bottom-right
///
/// Attributes:
///     position - the point the quad rotates around
///     corners - positions of the 4 corners, NOT relative to the quad position
///     colors - colors of the four corners
///     texture_coords - uv coordinates of the corners
///         (0, 0) => top-left, (0.5, 1) => horizontally in the middle, vertically at the bottom
///     position_env - index of the position envelope
///     position_env_offset - time offset of the position envelope, in ms
///     color_env - index of the color envelope
///     color_env_offset - time offset of the color envelope, in ms
#[pyclass(name = "Quad")]
#[derive(Clone)]
pub struct PyQuad(pub Arc<Mutex<Indexable<(), PyLayer>>>);

py_types!(PyQuad, PyQuads, (), PyLayer);

macro_rules! depy_array {
    ($function: ident, $value: ident, $py: ident) => {{
        let py_array: [_; 4] = $value.extract($py)?;
        let mapped: Vec<_> = py_array
            .iter()
            .map(|&e| $function(e))
            .collect::<Result<_, _>>()?;
        TryFrom::try_from(mapped).unwrap()
    }};
}

impl MapNavigating for Quad {
    type Entry = ();
    type Seq = PyLayer;
    type PyEntry = PyQuad;
    type PySeq = PyQuads;

    fn navigate_to_sequence<'a>(
        sequence: &SequenceIndex<Self::Entry, Self::Seq>,
        map: &'a mut TwMap,
    ) -> PyResult<&'a mut Vec<Self>> {
        let py_layer = sequence.predecessor.depythonize().lock().unwrap();
        let layer = Layer::navigate_to_object(&py_layer, map)?;
        Ok(&mut QuadsLayer::get_mut(layer).unwrap().quads)
    }

    fn getattr(element: &PyQuad, attr: &str, py: Python) -> PyResult<Option<PyObject>> {
        let py_quad = element.depythonize().lock().unwrap();
        let mut map = py_quad.map.lock().unwrap();
        let quad = Quad::navigate_to_object(&py_quad, &mut map)?;
        Ok(Some(match attr {
            "corners" => map_array(&quad.corners, point_to_tup).into_py(py),
            "position" => point_to_tup(quad.position).into_py(py),
            "colors" => map_array(&quad.colors, py_col).into_py(py),
            "texture_coords" => map_array(&quad.texture_coords, point_to_tup).into_py(py),
            "position_env" => quad.position_env.into_py(py),
            "position_env_offset" => quad.position_env_offset.into_py(py),
            "color_env" => quad.color_env.into_py(py),
            "color_env_offset" => quad.color_env_offset.into_py(py),
            _ => return Ok(None),
        }))
    }

    fn setattr(element: &PyQuad, attr: &str, value: PyObject, py: Python) -> PyResult<Option<()>> {
        let py_quad = element.depythonize().lock().unwrap();
        let mut map = py_quad.map.lock().unwrap();
        let quad = Quad::navigate_to_object(&py_quad, &mut map)?;
        match attr {
            "corners" => quad.corners = depy_array!(tup_to_point, value, py),
            "position" => quad.position = depy2(value, py)?,
            "colors" => quad.colors = depy_array!(col_from_tuple, value, py),
            "texture_coords" => quad.texture_coords = depy_array!(tup_to_point, value, py),
            "position_env" => quad.position_env = value.extract(py)?,
            "position_env_offset" => quad.position_env_offset = value.extract(py)?,
            "color_env" => quad.color_env = value.extract(py)?,
            "color_env_offset" => quad.color_env_offset = value.extract(py)?,
            _ => return Ok(None),
        }
        Ok(Some(()))
    }
}

do_entry_impls!(Quad, PyQuad, PyQuads,);

do_sequence_impls!(
    Quad,
    PyQuad,
    PyQuads,
    /// Constructs a new quad with the specified position, width and height and editor defaults.
    #[pyo3(name = "new")]
    fn py_new(&self, pos_x: f64, pos_y: f64, width: f64, height: f64) -> PyResult<PyQuad> {
        let position = tup_to_point((pos_x, pos_y))?;
        let size = tup_to_point((width, height))?;
        let new_quad = match Quad::new(position, size) {
            Some(q) => q,
            None => {
                return Err(pyo3::exceptions::PyValueError::new_err(
                    "Values too big, they cause an overflow",
                ))
            }
        };
        append_wrapped(self, new_quad)
    }
);

fn map_array<T: Copy, U: fmt::Debug, FN: Fn(T) -> U>(array: &[T; 4], function: FN) -> [U; 4] {
    let mapped: Vec<U> = array.iter().map(|&e| function(e)).collect();
    <[U; 4]>::try_from(mapped).unwrap()
}
