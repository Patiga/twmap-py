use twmap::{BezierCurve, BezierDefault, CurveKind, EnvPoint, Envelope, Position, TwMap, Volume};

use crate::envelopes::PyEnvelope;
use crate::sequence_wrapping::*;

use pyo3::prelude::*;
use pyo3::types::PyString;
use vek::{Rgba, Vec2};

use crate::{fixed_py, py_fixed};
use fixed::types::I22F10;
use std::borrow::Cow;
use std::sync::{Arc, Mutex};

fn py_curve<T>(curve: CurveKind<T>) -> &'static str {
    use CurveKind::*;
    match curve {
        Step => "Step",
        Linear => "Linear",
        Slow => "Slow",
        Fast => "Fast",
        Smooth => "Smooth",
        Bezier(_) => "Bezier",
        Unknown(_) => panic!("Unknown curve type not supported by python bindings"),
    }
}

fn depy_curve<T: EnvPointSequence + BezierDefault>(
    value: PyObject,
    py: Python,
) -> PyResult<CurveKind<T>> {
    use CurveKind::*;
    let kind: Cow<str> = value.extract(py)?;
    Ok(match kind.as_ref() {
        "Step" => Step,
        "Linear" => Linear,
        "Slow" => Slow,
        "Fast" => Fast,
        "Smooth" => Smooth,
        "Bezier" => Bezier(BezierCurve {
            handle_l: Vec2::new(T::bezier_default(), T::bezier_default()),
            handle_r: Vec2::new(T::bezier_default(), T::bezier_default()),
        }),
        _ => {
            return Err(pyo3::exceptions::PyValueError::new_err(
                "Curve kind not valid",
            ))
        }
    })
}

trait EnvPointSequence: Sized {
    fn extract_mut_vec(env: &mut Envelope) -> Option<&mut Vec<EnvPoint<Self>>>;

    fn pythonize(self, py: Python) -> PyObject;

    fn depythonize(value: PyObject, py: Python) -> PyResult<Self>;

    fn get_attr(&self, attr: &str) -> Option<f64>;

    fn set_attr(&mut self, attr: &str, value: &PyObject, py: Python) -> PyResult<bool>;
}

impl EnvPointSequence for Position {
    fn extract_mut_vec(env: &mut Envelope) -> Option<&mut Vec<EnvPoint<Self>>> {
        if let Envelope::Position(env) = env {
            Some(&mut env.points)
        } else {
            None
        }
    }

    fn pythonize(self, py: Python) -> PyObject {
        (
            fixed_py(self.offset.x),
            fixed_py(self.offset.y),
            fixed_py(self.rotation),
        )
            .into_py(py)
    }

    fn depythonize(value: PyObject, py: Python) -> PyResult<Self> {
        let (x, y, rotation) = value.extract(py)?;
        Ok(Position {
            offset: Vec2::new(py_fixed(x)?, py_fixed(y)?),
            rotation: py_fixed(rotation)?,
        })
    }

    fn get_attr(&self, attr: &str) -> Option<f64> {
        Some(match attr {
            "x" => fixed_py(self.offset.x),
            "y" => fixed_py(self.offset.y),
            "rotation" => fixed_py(self.rotation),
            _ => return None,
        })
    }

    fn set_attr(&mut self, attr: &str, value: &PyObject, py: Python) -> PyResult<bool> {
        match attr {
            "x" => self.offset.x = py_fixed(value.extract(py)?)?,
            "y" => self.offset.y = py_fixed(value.extract(py)?)?,
            "rotation" => self.rotation = py_fixed(value.extract(py)?)?,
            _ => return Ok(false),
        }
        Ok(true)
    }
}

impl EnvPointSequence for Rgba<I22F10> {
    fn extract_mut_vec(env: &mut Envelope) -> Option<&mut Vec<EnvPoint<Self>>> {
        if let Envelope::Color(env) = env {
            Some(&mut env.points)
        } else {
            None
        }
    }

    fn pythonize(self, py: Python) -> PyObject {
        (
            fixed_py(self.r),
            fixed_py(self.g),
            fixed_py(self.b),
            fixed_py(self.a),
        )
            .into_py(py)
    }

    fn depythonize(value: PyObject, py: Python) -> PyResult<Self> {
        let (r, g, b, a) = value.extract(py)?;
        Ok(Rgba {
            r: py_fixed(r)?,
            g: py_fixed(g)?,
            b: py_fixed(b)?,
            a: py_fixed(a)?,
        })
    }

    fn get_attr(&self, attr: &str) -> Option<f64> {
        Some(match attr {
            "r" => fixed_py(self.r),
            "g" => fixed_py(self.g),
            "b" => fixed_py(self.b),
            "a" => fixed_py(self.a),
            _ => return None,
        })
    }

    fn set_attr(&mut self, attr: &str, value: &PyObject, py: Python) -> PyResult<bool> {
        match attr {
            "r" => self.r = py_fixed(value.extract(py)?)?,
            "g" => self.g = py_fixed(value.extract(py)?)?,
            "b" => self.b = py_fixed(value.extract(py)?)?,
            "a" => self.a = py_fixed(value.extract(py)?)?,
            _ => return Ok(false),
        }
        Ok(true)
    }
}

impl EnvPointSequence for Volume {
    fn extract_mut_vec(env: &mut Envelope) -> Option<&mut Vec<EnvPoint<Self>>> {
        if let Envelope::Sound(env) = env {
            Some(&mut env.points)
        } else {
            None
        }
    }

    fn pythonize(self, py: Python) -> PyObject {
        fixed_py(self.0).into_py(py)
    }

    fn depythonize(value: PyObject, py: Python) -> PyResult<Self> {
        Ok(Volume(py_fixed(value.extract(py)?)?))
    }

    fn get_attr(&self, attr: &str) -> Option<f64> {
        if attr == "volume" {
            Some(fixed_py(self.0))
        } else {
            None
        }
    }

    fn set_attr(&mut self, attr: &str, value: &PyObject, py: Python) -> PyResult<bool> {
        if attr == "volume" {
            self.0 = py_fixed(value.extract(py)?)?;
            Ok(true)
        } else {
            Ok(false)
        }
    }
}

macro_rules! env_point_sequence {
    ($(#[$doc:meta])*,
        $inner_type: ty, $entry: ident, $seq: ident) => {
        #[pyclass]
        #[derive(Clone)]
        pub struct $seq(pub Arc<Mutex<SequenceIndex<(), PyEnvelope>>>);

        #[pyclass]
        /// Envelope point object that represents a point in the envelope graph.
        /// The content attribute is different for each kind of envelope!
        /// For all content types 1 = 1 tile or 1 = 100%, so the position can be used.
        /// Color channels have 1 = 100% of the color and position is in tiles, rotation is in degrees.
        ///
        /// Attributes:
        ///     time - Timestamp of the point, in ms
        $(#[$doc])*
        ///         All the separate values in content can be accessed as an attribute individually!
        ///     curve - how the graph connects this point to the next
        ///         possible values:
        ///             "Step" - constant value, abrupt value change at next point
        ///             "Linear" - linear increase to next point
        ///             "Slow" - starts to change slowly, then faster
        ///             "Fast" - starts to change fast, then slower
        ///             "Smooth" - starts slow, fast in the middle, then slow again
        ///             "Bezier" - very flexible, curve is defined by the handle attributes (see after)
        ///     If the curve is a bezier curve, four more attributes are available that contain a value for each channel:
        ///     Each of those attributes has the same values as content, one integer for each channel
        ///         handle_l_x
        ///         handle_l_y
        ///         handle_r_x
        ///         handle_r_y
        #[derive(Clone)]
        pub struct $entry(pub Arc<Mutex<Indexable<(), PyEnvelope>>>);

        py_types!($entry, $seq, (), PyEnvelope);

        impl MapNavigating for EnvPoint<$inner_type> {
            type Entry = ();
            type Seq = PyEnvelope;
            type PyEntry = $entry;
            type PySeq = $seq;

            fn navigate_to_sequence<'a>(sequence: &SequenceIndex<(), PyEnvelope>, map: &'a mut TwMap) -> PyResult<&'a mut Vec<Self>> {
                let py_envelope = sequence.predecessor.depythonize().lock().unwrap();
                let envelope = Envelope::navigate_to_object(&py_envelope, map)?;
                match <$inner_type>::extract_mut_vec(envelope) {
                    None => unreachable!("Internal error: Wrong envelope kind"),
                    Some(env_points) => Ok(env_points),
                }
            }

            fn getattr(element: &Self::PyEntry, attr: &str, py: Python) -> PyResult<Option<PyObject>> {
                let py_point = element.depythonize().lock().unwrap();
                let mut map = py_point.map.lock().unwrap();
                let env_point = EnvPoint::<$inner_type>::navigate_to_object(&py_point, &mut map)?;
                if let CurveKind::Bezier(b) = env_point.curve {
                    match attr {
                        "handle_l_x" => return Ok(Some(<$inner_type>::pythonize(b.handle_l.x, py))),
                        "handle_l_y" => return Ok(Some(<$inner_type>::pythonize(b.handle_l.y, py))),
                        "handle_r_x" => return Ok(Some(<$inner_type>::pythonize(b.handle_r.x, py))),
                        "handle_r_y" => return Ok(Some(<$inner_type>::pythonize(b.handle_r.y, py))),
                        _ => {}
                    }
                }
                Ok(Some(match attr {
                    "time" => env_point.time.into_py(py),
                    "content" => <$inner_type>::pythonize(env_point.content, py),
                    "curve" => py_curve(env_point.curve).into_py(py),
                    _ => return Ok(env_point.content.get_attr(attr).map(|n| n.into_py(py))),
                }))
            }

            fn setattr(element: &Self::PyEntry, attr: &str, value: PyObject, py: Python) -> PyResult<Option<()>> {
                let py_point = element.depythonize().lock().unwrap();
                let mut map = py_point.map.lock().unwrap();
                let env_point = EnvPoint::<$inner_type>::navigate_to_object(&py_point, &mut map)?;

                if let CurveKind::Bezier(b) = &mut env_point.curve {
                    let mut changed = true;
                    match attr {
                        "handle_l_x" => b.handle_l.x = <$inner_type>::depythonize(value.clone(), py)?,
                        "handle_l_y" => b.handle_l.y = <$inner_type>::depythonize(value.clone(), py)?,
                        "handle_r_x" => b.handle_r.x= <$inner_type>::depythonize(value.clone(), py)?,
                        "handle_r_y" => b.handle_r.y = <$inner_type>::depythonize(value.clone(), py)?,
                        _ => changed = false,
                    }
                    if changed {
                        return Ok(Some(()));
                    }
                }

                match attr {
                    "time" => env_point.time = value.extract(py)?,
                    "content" => env_point.content = <$inner_type>::depythonize(value, py)?,
                    "curve" => env_point.curve = depy_curve(value, py)?,
                    _ => {
                        if env_point.content.set_attr(attr, &value, py)? {
                            return Ok(Some(()))
                        } else {
                            return Ok(None)
                        }
                    },
                }
                Ok(Some(()))
            }
        }

        do_entry_impls!(EnvPoint<$inner_type>, $entry, $seq,);

        do_sequence_impls!(
            EnvPoint<$inner_type>,
            $entry,
            $seq,

            /// Construct a new envelope point with the specified timestamp.
            /// The point will placed right before the next highest one.
            #[pyo3(name = "new")]
            fn py_new(&self, time: i32) -> PyResult<$entry> {
                let new_env_point = EnvPoint::<$inner_type> {
                    time,
                    content: Default::default(),
                    curve: CurveKind::Linear,
                };
                let index = EnvPoint::<$inner_type>::access_sequence(self, |points| {
                    Ok(match points.iter().position(|point| point.time > new_env_point.time) {
                        Some(index) => index,
                        None => points.len(),
                    })
                })?;
                insert_wrapped_at(self, index, new_env_point)
            }
        );
    }
}

env_point_sequence!(#[doc="     content - (r, g, b, a) !Color envelope specific!"], Rgba<I22F10>, ColorEnvPoint, ColorEnvPoints);
env_point_sequence!(#[doc="     content - (x, y, rotation) !Position envelope specific!"], Position, PosEnvPoint, PosEnvPoints);
env_point_sequence!(#[doc="     content - volume !Sound envelope specific!"], Volume, SoundEnvPoint, SoundEnvPoints);
