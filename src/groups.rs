use twmap::{Group, TwMap};

use crate::layers::{LayerData, PyLayers};
use crate::sequence_wrapping::*;

use crate::{fixed_py, py_fixed};
use pyo3::{prelude::*, types::PyString};
use std::sync::{Arc, Mutex};

#[derive(Default)]
pub struct GroupData {
    layers: SequenceInitializer<LayerData, PyGroup>,
}

#[pyclass(name = "Groups")]
#[derive(Clone)]
pub struct PyGroups(pub Arc<Mutex<SequenceIndex<GroupData, ()>>>);

/// Group object that contains the layers of the map
/// It also offsets the layers and sets their parallax.
/// Clipping, if turned on, determines in which part of the map the layers will be visible.
///
/// Attributes:
///     name
///     layers - layers of this group
///     offset_x - how much the group is offset to the left. 1 = 1 tile
///     offset_y - how much the group is offset upwards
///     parallax_x - how fast this group moves on the x-axis relative to the physics group
///         100 -> same as physics group
///         0 -> stays in place
///     parallax_y - how fast this group moves on the y-axis relative to the physics group
///     clipping - if the group should only be visible in the rectangle defined by the next 4 attributes.
///         For all clip attributes: 1 = 1 tile
///         clip_x
///         clip_y
///         clip_width
///         clip_height
#[pyclass(name = "Group")]
#[derive(Clone)]
pub struct PyGroup(Arc<Mutex<Indexable<GroupData, ()>>>);

py_types!(PyGroup, PyGroups, GroupData, ());

impl MapNavigating for Group {
    type Entry = GroupData;
    type Seq = ();
    type PyEntry = PyGroup;
    type PySeq = PyGroups;

    fn navigate_to_sequence<'a>(
        _: &SequenceIndex<GroupData, ()>,
        map: &'a mut TwMap,
    ) -> PyResult<&'a mut Vec<Group>> {
        Ok(&mut map.groups)
    }

    fn getattr(element: &PyGroup, attr: &str, py: Python) -> PyResult<Option<PyObject>> {
        let mut py_group = element.depythonize().lock().unwrap();
        let arc_map = py_group.map.clone();
        let mut map = arc_map.lock().unwrap();
        let group = Group::navigate_to_object(&py_group, &mut map)?;
        Ok(Some(match attr {
            "name" => group.name.clone().into_py(py),
            "offset_x" => fixed_py(group.offset.x).into_py(py),
            "offset_y" => fixed_py(group.offset.y).into_py(py),
            "parallax_x" => group.parallax.x.into_py(py),
            "parallax_y" => group.parallax.y.into_py(py),
            "layers" => PyLayers(py_group.value.layers.get(
                group.layers.len(),
                arc_map.clone(),
                element.clone(),
            ))
            .into_py(py),
            "clipping" => group.clipping.into_py(py),
            "clip_x" => fixed_py(group.clip.x).into_py(py),
            "clip_y" => fixed_py(group.clip.y).into_py(py),
            "clip_width" => fixed_py(group.clip.w).into_py(py),
            "clip_height" => fixed_py(group.clip.h).into_py(py),
            _ => return Ok(None),
        }))
    }

    fn setattr(element: &PyGroup, attr: &str, value: PyObject, py: Python) -> PyResult<Option<()>> {
        let py_group = element.depythonize().lock().unwrap();
        let mut map = py_group.map.lock().unwrap();
        let group = Group::navigate_to_object(&py_group, &mut map)?;
        match attr {
            "name" => group.name = value.extract(py)?,
            "offset_x" => group.offset.x = py_fixed(value.extract(py)?)?,
            "offset_y" => group.offset.y = py_fixed(value.extract(py)?)?,
            "parallax_x" => group.parallax.x = value.extract(py)?,
            "parallax_y" => group.parallax.y = value.extract(py)?,
            "layers" => {
                return Err(pyo3::exceptions::PyAttributeError::new_err(
                    "Layers cannot be set, copy the group instead",
                ))
            }
            "clipping" => group.clipping = value.extract(py)?,
            "clip_x" => group.clip.x = py_fixed(value.extract(py)?)?,
            "clip_y" => group.clip.y = py_fixed(value.extract(py)?)?,
            "clip_width" => group.clip.w = py_fixed(value.extract(py)?)?,
            "clip_height" => group.clip.h = py_fixed(value.extract(py)?)?,
            _ => return Ok(None),
        };
        Ok(Some(()))
    }
}

do_entry_impls!(
    Group,
    PyGroup,
    PyGroups,
    /// Returns `True` if the group contains any physics layers
    fn is_physics_group(&self) -> PyResult<bool> {
        Group::access(self, |group| Ok(group.is_physics_group()))
    }
);

do_sequence_impls!(
    Group,
    PyGroup,
    PyGroups,
    /// Construct a new group with the default values
    #[pyo3(name = "new")]
    fn py_new(&self) -> PyResult<PyGroup> {
        let new_group = Group::default();
        append_wrapped(self, new_group)
    }
    /// Construct a new group with the physics group values
    fn new_physics(&self) -> PyResult<PyGroup> {
        let new_group = Group::physics();
        append_wrapped(self, new_group)
    }
);
