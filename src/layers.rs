use twmap::convert::TryTo;
use twmap::{FrontLayer, GameLayer, SpeedupLayer, SwitchLayer, TeleLayer, TilesLayer, TuneLayer};
use twmap::{
    Group, Layer, LayerKind, Load, QuadsLayer, SoundsLayer, TileFlags, TilemapLayer, TwMap,
};

use crate::groups::PyGroup;
use crate::quads::PyQuads;
use crate::sequence_wrapping::*;
use crate::{depy_col, map_err, py_col};

use ndarray::Array2;
use numpy::{PyArray, PyArray3, PyArrayMethods};
use pyo3::{prelude::*, types::PyString};
use vek::Extent2;

use std::collections::HashMap;
use std::sync::{Arc, Mutex};

#[derive(Default)]
pub struct LayerData {
    elements: SequenceInitializer<(), PyLayer>, // used for quads and also sounds layers
}

#[pyclass(name = "Layers")]
#[derive(Clone)]
pub struct PyLayers(pub Arc<Mutex<SequenceIndex<LayerData, PyGroup>>>);

/// Layer object which wraps all different layer kinds
/// Use the `kind` method to figure out which kind of layer it is.
///
/// Depending on the kind of layer different attributes will be available:
///     All layers:
///         name
///     Non-physics-layers:
///         detail - if this tiles layer is relevant for gameplay
///     Tilemap layers:
///         tiles - 3d numpy array with the dimension [height, width, n]. The last dimension varies on the layer kind:
///             'Tiles', 'Game', 'Front': [id, flags]
///             'Tele': [number, id]
///             'Speedup': [force, map_speed, id, angle]
///                 In 'Speedup', elements are i16 instead of u8, since angle can be anywhere between 0 and 360
///             'Switch': [number, id, flags, delay]
///             'Tune': [number, id]
///     'Tiles' layer:
///         color - color that is multiplied onto the image that is used
///         image - index of the image this layer uses
///         color_envelope - index of the color envelope
///         color_envelope_offset - time offset of the color envelope, in ms
///     'Quads' layer:
///         quads - quads of this layer
///         image - index of the image this layer uses
///     'Sounds' layer:
///         sources - sources of this layer
///         sound - index of the sound this layer uses
#[pyclass(name = "Layer")]
#[derive(Clone)]
pub struct PyLayer(Arc<Mutex<Indexable<LayerData, PyGroup>>>);
use crate::sequence_wrapping::PyEntry;
use crate::sound_sources::PySources;
py_types!(PyLayer, PyLayers, LayerData, PyGroup);

type Mesh = (Vec<(isize, isize)>, Vec<Vec<usize>>, Vec<[(f32, f32); 4]>);

impl MapNavigating for Layer {
    type Entry = LayerData;
    type Seq = PyGroup;
    type PyEntry = PyLayer;
    type PySeq = PyLayers;

    fn navigate_to_sequence<'a>(
        sequence: &SequenceIndex<LayerData, PyGroup>,
        map: &'a mut TwMap,
    ) -> PyResult<&'a mut Vec<Layer>> {
        let py_group = sequence.predecessor.depythonize().lock().unwrap();
        let group = Group::navigate_to_object(&py_group, map)?;
        Ok(&mut group.layers)
    }

    fn getattr(element: &PyLayer, attr: &str, py: Python) -> PyResult<Option<PyObject>> {
        let mut py_layer = element.depythonize().lock().unwrap();
        let arc_map = py_layer.map.clone();
        let mut map = arc_map.lock().unwrap();
        let layer = Layer::navigate_to_object(&py_layer, &mut map)?;
        use Layer::*;
        if attr == "name" {
            return Ok(Some(layer.name().into_py(py)));
        }
        Ok(Some(match (attr, layer) {
            ("tiles", Game(l)) => l.tiles_to_py_array(py)?.into_py(py),
            ("tiles", Tiles(l)) => l.tiles_to_py_array(py)?.into_py(py),
            ("tiles", Front(l)) => l.tiles_to_py_array(py)?.into_py(py),
            ("tiles", Tele(l)) => l.tiles_to_py_array(py)?.into_py(py),
            ("tiles", Speedup(l)) => l.tiles_to_py_array(py)?.into_py(py),
            ("tiles", Switch(l)) => l.tiles_to_py_array(py)?.into_py(py),
            ("tiles", Tune(l)) => l.tiles_to_py_array(py)?.into_py(py),

            ("detail", Quads(l)) => l.detail.into_py(py),
            ("detail", Tiles(l)) => l.detail.into_py(py),
            ("detail", Sounds(l)) => l.detail.into_py(py),

            ("image", Quads(l)) => l.image.into_py(py),
            ("image", Tiles(l)) => l.image.into_py(py),
            ("sound", Sounds(l)) => l.sound.into_py(py),

            ("color", Tiles(l)) => py_col(l.color).into_py(py),
            ("color_env", Tiles(l)) => l.color_env.into_py(py),
            ("color_env_offset", Tiles(l)) => l.color_env.into_py(py),
            ("quads", Quads(l)) => PyQuads(py_layer.value.elements.get(
                l.quads.len(),
                arc_map.clone(),
                element.clone(),
            ))
            .into_py(py),
            ("sources", Sounds(l)) => PySources(py_layer.value.elements.get(
                l.sources.len(),
                arc_map.clone(),
                element.clone(),
            ))
            .into_py(py),
            _ => return Ok(None),
        }))
    }

    fn setattr(element: &PyLayer, attr: &str, value: PyObject, py: Python) -> PyResult<Option<()>> {
        let py_layer = element.depythonize().lock().unwrap();
        let mut map = py_layer.map.lock().unwrap();
        let layer = Layer::navigate_to_object(&py_layer, &mut map)?;
        use Layer::*;
        if attr == "name" {
            return match layer.name_mut() {
                None => Err(pyo3::exceptions::PyAttributeError::new_err(
                    "The names of physics layers are static",
                )),
                Some(name) => {
                    *name = value.extract(py)?;
                    Ok(Some(()))
                }
            };
        }
        match (attr, layer) {
            ("tiles", Game(l)) => l.tiles_from_py_array(value, py)?,
            ("tiles", Tiles(l)) => l.tiles_from_py_array(value, py)?,
            ("tiles", Front(l)) => l.tiles_from_py_array(value, py)?,
            ("tiles", Tele(l)) => l.tiles_from_py_array(value, py)?,
            ("tiles", Speedup(l)) => l.tiles_from_py_array(value, py)?,
            ("tiles", Switch(l)) => l.tiles_from_py_array(value, py)?,
            ("tiles", Tune(l)) => l.tiles_from_py_array(value, py)?,

            ("detail", Quads(l)) => l.detail = value.extract(py)?,
            ("detail", Tiles(l)) => l.detail = value.extract(py)?,
            ("detail", Sounds(l)) => l.detail = value.extract(py)?,

            ("image", Quads(l)) => l.image = value.extract(py)?,
            ("image", Tiles(l)) => l.image = value.extract(py)?,
            ("sound", Sounds(l)) => l.sound = value.extract(py)?,

            ("color", Tiles(l)) => l.color = depy_col(value, py)?,
            ("color_env", Tiles(l)) => l.color_env = value.extract(py)?,
            ("color_env_offset", Tiles(l)) => l.color_env = value.extract(py)?,
            ("quads", Quads(_)) => {
                return Err(pyo3::exceptions::PyAttributeError::new_err(
                    "Quads cannot be set, copy the layer instead",
                ))
            }
            ("sources", Sounds(_)) => {
                return Err(pyo3::exceptions::PyAttributeError::new_err(
                    "Sources cannot be set, copy the layer instead",
                ))
            }
            _ => return Ok(None),
        }
        Ok(Some(()))
    }
}

do_entry_impls!(
    Layer,
    PyLayer,
    PyLayers,
    /// Returns the width of tile map layers
    fn width(&self) -> PyResult<usize> {
        Layer::access(self, |layer| match layer.shape() {
            None => Err(pyo3::exceptions::PyValueError::new_err(
                "Not a tile map layer",
            )),
            Some(shape) => Ok(shape.w),
        })
    }
    /// Returns the height of tile map layers
    fn height(&self) -> PyResult<usize> {
        Layer::access(self, |layer| match layer.shape() {
            None => Err(pyo3::exceptions::PyValueError::new_err(
                "Not a tile map layer",
            )),
            Some(shape) => Ok(shape.h),
        })
    }
    fn kind(&self) -> PyResult<&'static str> {
        use LayerKind::*;
        Layer::access(self, |layer| {
            Ok(match layer.kind() {
                Game => "Game",
                Tiles => "Tiles",
                Quads => "Quads",
                Front => "Front",
                Tele => "Tele",
                Speedup => "Speedup",
                Switch => "Switch",
                Tune => "Tune",
                Sounds => "Sounds",
                Invalid(_) => {
                    return Err(pyo3::exceptions::PyValueError::new_err(
                        "Invalid layer type",
                    ))
                }
            })
        })
    }
    /// Only works with tiles layers
    /// Returns the mesh representation of the layer alongside the uv-mapping
    /// Return type is (vertices, faces, faces_uvs)
    ///     - vertices is a list of (x, y) coordinate tuples
    ///     - faces is a list of lists of indices into the vertices list. Each list represents one tile (indices 4 vertices)
    ///     - faces_uvs is a list of lists of (x, y) uv coordinates. Each list is for one tile (4 coordinates)
    fn to_mesh(&self) -> PyResult<Mesh> {
        Layer::access(self, |layer| {
            let tiles = {
                if let Layer::Tiles(l) = layer {
                    l.tiles.load().map_err(map_err)?;
                    l.tiles.unwrap_ref()
                } else {
                    return Err(pyo3::exceptions::PyValueError::new_err("Not a tiles layer"));
                }
            };
            let mut verts = Vec::new();
            let mut faces = Vec::new();
            let mut faces_uvs = Vec::new();
            let mut verts_index: HashMap<(isize, isize), usize> = HashMap::new();

            for ((y, x), tile) in tiles.indexed_iter() {
                if tile.id == 0 {
                    continue;
                }
                let (y, x): (isize, isize) = (y.try_to(), x.try_to());
                let mut face: Vec<usize> = Vec::new();
                // top-left -> top-right -> bottom-right -> bottom-left
                for (dx, dy) in [(0, 0), (1, 0), (1, 1), (0, 1)].iter() {
                    let new_vert = (x + *dx, y + *dy);
                    verts_index.entry(new_vert).or_insert_with(|| {
                        verts.push((new_vert.0, -new_vert.1)); // we build the map in +x, -y from the origin
                        verts.len() - 1
                    });
                    face.push(verts_index[&new_vert]);
                }
                faces.push(face);

                // note that the y-axis is inverted in uv-maps, so the coordinates are treated differently
                let uv_row = 15 - tile.id / 16;
                let uv_col = tile.id % 16;
                let mut uvs: [(f32, f32); 4] = [(0, 1), (1, 1), (1, 0), (0, 0)]
                    .map(|(dx, dy)| ((uv_col + dx) as f32 / 16.0, (uv_row + dy) as f32 / 16.0));
                if tile.flags.contains(TileFlags::FLIP_X) {
                    uvs = [uvs[1], uvs[0], uvs[3], uvs[2]];
                }
                if tile.flags.contains(TileFlags::FLIP_Y) {
                    uvs.reverse();
                }
                if tile.flags.contains(TileFlags::ROTATE) {
                    uvs = [uvs[3], uvs[0], uvs[1], uvs[2]]
                }
                faces_uvs.push(uvs);
            }
            Ok((verts, faces, faces_uvs))
        })
    }
);

do_sequence_impls!(
    Layer,
    PyLayer,
    PyLayers,
    /// Constructs a quads layer.
    fn new_quads(&self) -> PyResult<PyLayer> {
        let new_layer = Layer::Quads(QuadsLayer::default());
        append_wrapped(self, new_layer)
    }
    /// Constructs a quads layer.
    fn new_sounds(&self) -> PyResult<PyLayer> {
        let new_layer = Layer::Sounds(SoundsLayer::default());
        append_wrapped(self, new_layer)
    }
    /// Constructs a tiles layer.
    fn new_tiles(&self, width: usize, height: usize) -> PyResult<PyLayer> {
        let new_layer = Layer::Tiles(TilesLayer::new((height, width)));
        append_wrapped(self, new_layer)
    }
    /// Constructs a game layer.
    fn new_game(&self, width: usize, height: usize) -> PyResult<PyLayer> {
        let new_layer = Layer::Game(GameLayer {
            tiles: Array2::default((height, width)).into(),
        });
        append_wrapped(self, new_layer)
    }
    /// Constructs the specified physics layer.
    /// Note that the dimension of the tiles will be taken from the already existing physics layers in the same group.
    /// If there isn't a physics layer in the same group, this action will fail.
    fn new_physics(&self, kind: &str) -> PyResult<PyLayer> {
        let shape = Layer::access_sequence(self, |layers| {
            let dimensions: Vec<Extent2<usize>> = layers
                .iter()
                .filter(|layer| layer.kind().is_physics_layer())
                .map(|layer| layer.shape().unwrap())
                .collect();
            if dimensions.is_empty() {
                Err(pyo3::exceptions::PyLookupError::new_err(
                    "No other physics layers found in group",
                ))
            } else if dimensions.iter().any(|&dim| dim != dimensions[0]) {
                Err(pyo3::exceptions::PyLookupError::new_err(
                    "The other physics layers have an inconsistent size",
                ))
            } else {
                Ok(dimensions[0])
            }
        })?;
        let shape = (shape.h, shape.w);
        let new_layer = match kind {
            "Game" => Layer::Game(GameLayer {
                tiles: Array2::default(shape).into(),
            }),
            "Front" => Layer::Front(FrontLayer {
                tiles: Array2::default(shape).into(),
            }),
            "Tele" => Layer::Tele(TeleLayer {
                tiles: Array2::default(shape).into(),
            }),
            "Speedup" => Layer::Speedup(SpeedupLayer {
                tiles: Array2::default(shape).into(),
            }),
            "Switch" => Layer::Switch(SwitchLayer {
                tiles: Array2::default(shape).into(),
            }),
            "Tune" => Layer::Tune(TuneLayer {
                tiles: Array2::default(shape).into(),
            }),
            "Tiles" | "Quads" | "Sounds" => {
                return Err(pyo3::exceptions::PyValueError::new_err(
                    "Not a physics layer kind",
                ))
            }
            _ => return Err(pyo3::exceptions::PyValueError::new_err("Not a layer kind")),
        };
        append_wrapped(self, new_layer)
    }
);

trait PyTilemapLayer: TilemapLayer {
    type ByteType: numpy::Element;

    const BYTES_PER_TILE: usize;

    fn tile_to_py_bytes(tile: &Self::TileType) -> Vec<Self::ByteType>;

    fn load(&mut self) -> PyResult<()>;

    fn tiles_to_py_array(&mut self, py: Python) -> PyResult<Py<PyArray3<Self::ByteType>>> {
        self.load()?;
        let tiles = self.tiles().unwrap_ref();
        let dim = tiles.dim();
        let tiles_in_bytes: Vec<Self::ByteType> =
            tiles.iter().flat_map(Self::tile_to_py_bytes).collect();
        let py_tiles_array = PyArray::from_slice_bound(py, &tiles_in_bytes);
        Ok(py_tiles_array
            .reshape((dim.0, dim.1, Self::BYTES_PER_TILE))?
            .into())
    }

    fn tile_from_py_bytes(bytes: &[Self::ByteType]) -> PyResult<Self::TileType>;

    fn tiles_from_py_array(&mut self, value: PyObject, py: Python) -> PyResult<()> {
        let py_array3: &PyArray3<Self::ByteType> = value.extract(py)?;
        let dims = py_array3.shape();
        if dims[2] != Self::BYTES_PER_TILE {
            return Err(pyo3::exceptions::PyValueError::new_err(format!(
                "The last dimension of the layer must be of size {} for {:?} layers",
                Self::BYTES_PER_TILE,
                Self::kind()
            )));
        }
        let (height, width) = (dims[0], dims[1]);
        let tile_data: Vec<Self::ByteType> = match py_array3.readonly().as_slice() {
            Ok(slice) => slice.to_owned(),
            Err(_) => {
                return Err(pyo3::exceptions::PyValueError::new_err(
                    "The array is not contiguous",
                ))
            }
        };
        let vec_tiles: Vec<Self::TileType> = tile_data
            .chunks(Self::BYTES_PER_TILE)
            .map(Self::tile_from_py_bytes)
            .collect::<PyResult<_>>()?;
        let array_tiles = Array2::from_shape_vec((height, width), vec_tiles).unwrap();
        *self.tiles_mut() = array_tiles.into();
        Ok(())
    }
}

impl PyTilemapLayer for GameLayer {
    type ByteType = u8;

    const BYTES_PER_TILE: usize = 2;

    fn tile_to_py_bytes(tile: &Self::TileType) -> Vec<u8> {
        vec![tile.id, tile.flags.bits()]
    }

    fn load(&mut self) -> PyResult<()> {
        self.tiles.load().map_err(map_err)
    }

    fn tile_from_py_bytes(bytes: &[Self::ByteType]) -> PyResult<Self::TileType> {
        Ok(twmap::GameTile::new(
            bytes[0],
            TileFlags::from_bits_retain(bytes[1]),
        ))
    }
}

impl PyTilemapLayer for TilesLayer {
    type ByteType = u8;

    const BYTES_PER_TILE: usize = 2;

    fn tile_to_py_bytes(tile: &Self::TileType) -> Vec<u8> {
        vec![tile.id, tile.flags.bits()]
    }

    fn load(&mut self) -> PyResult<()> {
        self.tiles.load().map_err(map_err)
    }

    fn tile_from_py_bytes(bytes: &[Self::ByteType]) -> PyResult<Self::TileType> {
        Ok(twmap::Tile::new(
            bytes[0],
            TileFlags::from_bits_retain(bytes[1]),
        ))
    }
}

impl PyTilemapLayer for FrontLayer {
    type ByteType = u8;

    const BYTES_PER_TILE: usize = 2;

    fn tile_to_py_bytes(tile: &Self::TileType) -> Vec<u8> {
        vec![tile.id, tile.flags.bits()]
    }

    fn load(&mut self) -> PyResult<()> {
        self.tiles.load().map_err(map_err)
    }

    fn tile_from_py_bytes(bytes: &[Self::ByteType]) -> PyResult<Self::TileType> {
        Ok(twmap::GameTile::new(
            bytes[0],
            TileFlags::from_bits_retain(bytes[1]),
        ))
    }
}

impl PyTilemapLayer for TeleLayer {
    type ByteType = u8;

    const BYTES_PER_TILE: usize = 2;

    fn tile_to_py_bytes(tile: &Self::TileType) -> Vec<u8> {
        vec![tile.number, tile.id]
    }

    fn load(&mut self) -> PyResult<()> {
        self.tiles.load().map_err(map_err)
    }

    fn tile_from_py_bytes(bytes: &[Self::ByteType]) -> PyResult<Self::TileType> {
        Ok(twmap::Tele {
            number: bytes[0],
            id: bytes[1],
        })
    }
}

impl PyTilemapLayer for SpeedupLayer {
    type ByteType = i16;

    const BYTES_PER_TILE: usize = 4;

    fn tile_to_py_bytes(tile: &Self::TileType) -> Vec<i16> {
        vec![
            tile.force.into(),
            tile.max_speed.into(),
            tile.id.into(),
            tile.angle.into(),
        ]
    }

    fn load(&mut self) -> PyResult<()> {
        self.tiles.load().map_err(map_err)
    }

    fn tile_from_py_bytes(bytes: &[Self::ByteType]) -> PyResult<Self::TileType> {
        if bytes[0..3].iter().any(|&val| val > 255) {
            return Err(pyo3::exceptions::PyValueError::new_err(
                "id, force and max_speed must not be larger than 255",
            ));
        }
        Ok(twmap::Speedup::new(
            bytes[0].try_to(),
            bytes[1].try_to(),
            bytes[2].try_to(),
            bytes[3],
        ))
    }
}

impl PyTilemapLayer for SwitchLayer {
    type ByteType = u8;

    const BYTES_PER_TILE: usize = 4;

    fn tile_to_py_bytes(tile: &Self::TileType) -> Vec<u8> {
        vec![tile.number, tile.id, tile.flags.bits(), tile.delay]
    }

    fn load(&mut self) -> PyResult<()> {
        self.tiles.load().map_err(map_err)
    }

    fn tile_from_py_bytes(bytes: &[Self::ByteType]) -> PyResult<Self::TileType> {
        Ok(twmap::Switch {
            number: bytes[0],
            id: bytes[1],
            flags: TileFlags::from_bits_retain(bytes[2]),
            delay: bytes[3],
        })
    }
}

impl PyTilemapLayer for TuneLayer {
    type ByteType = u8;

    const BYTES_PER_TILE: usize = 2;

    fn tile_to_py_bytes(tile: &Self::TileType) -> Vec<u8> {
        vec![tile.number, tile.id]
    }

    fn load(&mut self) -> PyResult<()> {
        self.tiles.load().map_err(map_err)
    }

    fn tile_from_py_bytes(bytes: &[Self::ByteType]) -> PyResult<Self::TileType> {
        Ok(twmap::Tune {
            number: bytes[0],
            id: bytes[1],
        })
    }
}
