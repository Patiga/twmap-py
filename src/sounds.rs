use twmap::{Load, Sound, TwMap};

use crate::map_err;
use crate::sequence_wrapping::*;

use pyo3::prelude::*;
use pyo3::types::PyString;

use std::sync::{Arc, Mutex};

#[pyclass(name = "Sounds")]
#[derive(Clone)]
pub struct PySounds(pub Arc<Mutex<SequenceIndex<(), ()>>>);

/// Sound object containing the data of a map sound
/// The sound data is in the opus format.
/// Writing the data directly into an .opus file allows you to listen to it.
///
/// Attributes:
///     name
///     data - sound data as a list of bytes
#[pyclass(name = "Sound")]
#[derive(Clone)]
pub struct PySound(pub Arc<Mutex<Indexable<(), ()>>>);

fn py_opus_err(err: opus_headers::ParseError) -> PyErr {
    match err {
        opus_headers::ParseError::Io(err) => pyo3::exceptions::PyIOError::new_err(err.to_string()),
        _ => pyo3::exceptions::PyValueError::new_err(err.to_string()),
    }
}

py_types!(PySound, PySounds, (), ());

impl MapNavigating for Sound {
    type Entry = ();
    type Seq = ();
    type PyEntry = PySound;
    type PySeq = PySounds;

    fn navigate_to_sequence<'a>(
        _: &SequenceIndex<(), ()>,
        map: &'a mut TwMap,
    ) -> PyResult<&'a mut Vec<Sound>> {
        Ok(&mut map.sounds)
    }

    fn getattr(element: &PySound, attr: &str, py: Python) -> PyResult<Option<PyObject>> {
        let py_sound = element.depythonize().lock().unwrap();
        let mut map = py_sound.map.lock().unwrap();
        let sound = Sound::navigate_to_object(&py_sound, &mut map)?;
        Ok(Some(match attr {
            "name" => sound.name.clone().into_py(py),
            "data" => {
                sound.data.load().map_err(map_err)?;
                sound.data.unwrap_ref().clone().into_py(py)
            }
            _ => return Ok(None),
        }))
    }

    fn setattr(element: &PySound, attr: &str, value: PyObject, py: Python) -> PyResult<Option<()>> {
        let py_sound = element.depythonize().lock().unwrap();
        let mut map = py_sound.map.lock().unwrap();
        let sound = Sound::navigate_to_object(&py_sound, &mut map)?;
        match attr {
            "name" => sound.name = value.extract(py)?,
            "data" => {
                let data = value.extract::<Vec<u8>>(py)?;
                opus_headers::parse_from_read(&data[..]).map_err(py_opus_err)?;
                sound.data = data.into();
            }
            _ => return Ok(None),
        }
        Ok(Some(()))
    }
}

do_entry_impls!(Sound, PySound, PySounds,);

do_sequence_impls!(
    Sound,
    PySound,
    PySounds,
    /// Constructs a sound from an opus file.
    fn new_from_file(&self, path: &str) -> PyResult<PySound> {
        let new_sound = match Sound::from_file(path) {
            Ok(sound) => sound,
            Err(err) => return Err(py_opus_err(err)),
        };
        append_wrapped(self, new_sound)
    }
    /// Constructs a sound with the specified name and data.
    fn new_from_data(&self, name: &str, data: Vec<u8>) -> PyResult<PySound> {
        opus_headers::parse_from_read(&data[..]).map_err(py_opus_err)?;
        let new_sound = Sound {
            name: name.to_owned(),
            data: data.into(),
        };
        append_wrapped(self, new_sound)
    }
);
