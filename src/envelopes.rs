use twmap::{Env, Envelope, TwMap};

use crate::envelope_points::{ColorEnvPoints, PosEnvPoints, SoundEnvPoints};
use crate::sequence_wrapping::*;

use pyo3::{prelude::*, types::PyString};

use crate::SequenceInitializer;
use std::sync::{Arc, Mutex};

#[derive(Default)]
pub struct EnvelopeData {
    env_points: SequenceInitializer<(), PyEnvelope>,
}

#[pyclass(name = "Envelopes")]
#[derive(Clone)]
pub struct PyEnvelopes(pub Arc<Mutex<SequenceIndex<EnvelopeData, ()>>>);

/// Envelope object that wraps the three different envelope kinds
/// Use the `kind` method to figure out which kind of envelope it is.
/// The attributes are the same either way, but different kinds of envelope points will be returned.
///
/// Attributes:
///     name
///     points - envelope points of this envelope that define the graph
///     synchronized - if the envelope loop should be synced to server time (else it syncs with client time)
#[pyclass(name = "Envelope")]
#[derive(Clone)]
pub struct PyEnvelope(pub Arc<Mutex<Indexable<EnvelopeData, ()>>>);

py_types!(PyEnvelope, PyEnvelopes, EnvelopeData, ());

impl MapNavigating for Envelope {
    type Entry = EnvelopeData;
    type Seq = ();
    type PyEntry = PyEnvelope;
    type PySeq = PyEnvelopes;

    fn navigate_to_sequence<'a>(
        _: &SequenceIndex<EnvelopeData, ()>,
        map: &'a mut TwMap,
    ) -> PyResult<&'a mut Vec<Envelope>> {
        Ok(&mut map.envelopes)
    }

    fn getattr(element: &PyEnvelope, attr: &str, py: Python) -> PyResult<Option<PyObject>> {
        let mut py_envelope = element.depythonize().lock().unwrap();
        let arc_map = py_envelope.map.clone();
        let mut map = arc_map.lock().unwrap();
        let envelope = Envelope::navigate_to_object(&py_envelope, &mut map)?;
        Ok(Some(match attr {
            "name" => envelope.name().clone().into_py(py),
            "synchronized" => match envelope {
                Envelope::Position(env) => env.synchronized,
                Envelope::Color(env) => env.synchronized,
                Envelope::Sound(env) => env.synchronized,
            }
            .into_py(py),
            "points" => match envelope {
                Envelope::Position(env) => PosEnvPoints(py_envelope.value.env_points.get(
                    env.points.len(),
                    arc_map.clone(),
                    element.clone(),
                ))
                .into_py(py),
                Envelope::Color(env) => ColorEnvPoints(py_envelope.value.env_points.get(
                    env.points.len(),
                    arc_map.clone(),
                    element.clone(),
                ))
                .into_py(py),
                Envelope::Sound(env) => SoundEnvPoints(py_envelope.value.env_points.get(
                    env.points.len(),
                    arc_map.clone(),
                    element.clone(),
                ))
                .into_py(py),
            },
            _ => return Ok(None),
        }))
    }

    fn setattr(
        element: &PyEnvelope,
        attr: &str,
        value: PyObject,
        py: Python,
    ) -> PyResult<Option<()>> {
        let py_envelope = element.depythonize().lock().unwrap();
        let mut map = py_envelope.map.lock().unwrap();
        let envelope = Envelope::navigate_to_object(&py_envelope, &mut map)?;
        match attr {
            "name" => *envelope.name_mut() = value.extract(py)?,
            "synchronized" => match envelope {
                Envelope::Position(env) => env.synchronized = value.extract(py)?,
                Envelope::Color(env) => env.synchronized = value.extract(py)?,
                Envelope::Sound(env) => env.synchronized = value.extract(py)?,
            },
            "points" => {
                return Err(pyo3::exceptions::PyAttributeError::new_err(
                    "Points cannot be set, copy the envelope instead",
                ))
            }
            _ => return Ok(None),
        }
        Ok(Some(()))
    }
}

do_entry_impls!(
    Envelope,
    PyEnvelope,
    PyEnvelopes,
    fn kind(&self) -> PyResult<&'static str> {
        Envelope::access(self, |env| {
            Ok(match env {
                Envelope::Position(_) => "Position",
                Envelope::Color(_) => "Color",
                Envelope::Sound(_) => "Sound",
            })
        })
    }
);

do_sequence_impls!(
    Envelope,
    PyEnvelope,
    PyEnvelopes,
    /// Construct the specified envelope kind with default values.
    /// The parameter 'kind' must be one of "Position", "Color", "Sound".
    #[pyo3(name = "new")]
    fn py_new(&self, kind: &str) -> PyResult<PyEnvelope> {
        let new_env = match kind {
            "Position" => Envelope::Position(Env::default()),
            "Color" => Envelope::Color(Env::default()),
            "Sound" => Envelope::Sound(Env::default()),
            _ => {
                return Err(pyo3::exceptions::PyValueError::new_err(
                    "Invalid envelope kind",
                ))
            }
        };
        append_wrapped(self, new_env)
    }
);
